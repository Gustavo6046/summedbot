use std::string::String;
use std::net::{SocketAddr, TcpStream};
use std::result::Result;
use std::time::Duration;
use std::rc::Rc;
use std::io::Read;
use std::collections::HashMap;



/// A single structure encompassing all the IRC connections of a
/// given application.
pub struct Bot {
    /// A list of IRC connections.
    pub connections:        Vec<Rc<Connection>>,

    /// A map of connection names to the connections themselves.
    pub connectionLookup:   HashMap<str, Rc<Connection>>,

    /// A list of EventRepresenters.
    pub representers:       Vec<Rc<EventRepresenter>>,

    /// A list of EventHandlers.
    pub handlers:           Vec<Rc<EventHandler>>,
}

impl Bot {
    /// Parse an event into Representations of it, then proccess
    /// those.
    pub fn parse(&self, evt: Rc<Event>) {
        for repr in self.representers {
            if repr.canRepresent(evt) {
                let erepr = EventRepresentation {
                    core: EventRepresentationCore {
                        source: evt,
                        representer: repr,
                    },
                    data: HashMap::new(),
                };
                repr.addData(erepr);

                self.emit(Rc::from(erepr));
            }
        }
    }

    /// Processes (emits) a single event representation.
    pub fn emit(&self, evt: Rc<EventRepresentation>) {
        for handler in self.handlers {
            if handler.canHandle(self, evt) {
                
            }
        }
    }

    fn _wrap_evt(&self, conn_name: String, line: String) -> Event {
        return Event {
            line: line,
            from: Rc::clone(self.connectionLookup.get(conn_name.as_str()).expect("Tried to wrap an event from a connection that doesn't exist!"))
        };
    }
}


/// A single IRC connection/socket.
/// 
/// This struct contains enough data to represent and
/// abstract a socket connected to an IRC server, along
/// with some extra metadata (such as the high-level name
/// of this connection).
pub struct Connection {
    /// The high-level name of this connection.
    /// Used to identify it in a variety of scenarios.
    pub name:   String,

    /// The address of the connection's server.
    pub addr:   SocketAddr,

    /// The connection's socket, if any.
    pub socket: TcpStream,

    /// The bot owning this connection.
    pub bot:    Rc<Bot>,

    lineBuffer: String,
}

impl Connection {
    /// Creates a new connection to an IRC server.
    pub fn new(bot: Rc<Bot>, name: String, address: SocketAddr, timeout: u64) -> Result<Connection, String> {
        let mut sock: std::io::Result<TcpStream>;

        if timeout > 0 {
            sock = TcpStream::connect_timeout(&address, Duration::from_secs(timeout));
        }

        else {
            sock = TcpStream::connect(&address);
        }

        match sock {
            Ok(stream) =>
                return Ok(Connection {
                    name: name,
                    addr: address,
                    socket: stream,
                    lineBuffer: "".to_owned(),
                    bot: bot,
                }),
                            
            Err(e) => 
                return Err(
                    format!("Error connecting to server {0}! ({1})", address, e)
                ),
        };
    }

    /// Ticks this connection, i.e. processes all inputs exactly once,
    /// reading since the last time it was stepped, until it reads all data
    /// available by then.
    pub fn step(&self) -> Result<(), String> {
        let mut buf = vec![];
        
        if let Err(oops) = self.socket.read_to_end(&mut buf) {
            return Err(
                format!("Error reading IRC data from TCP stream! ({0})", oops)
            );
        }
        
        let mut bufStr: String = self.lineBuffer;

        match String::from_utf8(buf) {
            Ok(data) =>
                bufStr.push_str(data.as_str()),
                
            Err(notData) => {
                return Err(
                    format!("Error reading IRC data as UTF-8! ({0})", notData)
                );
            }
        }

        let mut allLines: Vec<String> = bufStr.split("\n").collect();

        if (allLines.len() > 0) {
            self.lineBuffer = allLines.pop().expect("A vector with non-zero length's last() method returned None!");

            for line in allLines {
                self.bot.parse(Rc::from(self.bot._wrap_evt(self.name, line)));
            }
        }

        else {
            self.lineBuffer = "".to_owned();
        }

        Ok(());
    }
}

/// A single event that has happened on IRC. That is,
/// any one-time inbound information that has been
/// received from the remote IRC server.
/// 
/// For example, messages sent to channels (or directly
/// to the client) are called .
/// 
/// This is a low-level representation of IRC events.
/// For higher levels, see EventRepresentation.
pub struct Event {
    /// The original line read from the underlying TCP stream
    /// of the IRC connection.
    pub line:   String,

    /// The connection from the which this event was read.
    pub from:   Rc<Connection>,
}

impl Event {
    pub fn new(from: Rc<Connection>, data: String) -> Event {
        return Event {
            line: data,
            from: from.clone(),
        };
    }
}


pub struct EventRepresentationCore {
    /// The low-level Event that was parsed into this representation.
    pub source:         Rc<Event>,

    /// The type of this representation.
    pub representer:    Rc<EventRepresenter<T>>,
}

/// Any type that can make an EventRepresentation out of an Event.
pub trait EventRepresenter {
    /// Whether this event can be parsed by this Representer.
    fn canRepresent(&self, evt: Rc<Event>) -> bool;

    /// Should extract data from the event's source, then add to its data
    /// fields.
    fn addData(&self, &mut evt: EventRepresentation);
}

pub trait EventHandler {
    fn canHandle(&self, bot: &Bot, evt: Rc<EventRepresentation>) -> bool;
    fn handle(&self, bot: &Bot, evt: Rc<EventRepresentation>);
}

/// Types that can represent an IRC event in a high-level
/// manner.
pub struct EventRepresentation {
    pub core:       EventRepresentationCore,
    pub data:       HashMap<String, String>,
}

impl EventRepresentation {
    pub fn set(&self, String key, String val) {
        self.
    }
}